require_relative 'window'

class Hyundai
	attr_reader :background_image, :abs, :air_bag, :on_sis, :arka_sis, :turn_left,
				 :turn_right, :flashers, :dipped_beam, :main_beam, :daytime_running_lamp,
				 :handbrake_indicator, :park_lights, :belt, :title
	def initialize(window, digit_font, string_font, background_image)
		@timers = Timers::Group.new
		@title = "Hyundai Accent 3DR HB"
		@background_image = background_image
		@digit_font = digit_font
		@string_font = string_font
		@start_angle_speed = -127.0
		@angle_speed = @start_angle_speed
		@start_angle_fuel = 10.0
		@angle_fuel = @start_angle_fuel
		@speed = 0
		@distance = 0
		@handbrake_toggle = 0
		@angle_heat = -70
		@angle_round = -127.0
		init_indicators(window)
		init_gosu_objects(window, @digit_font, @string_font)
	end
	def init_indicators(window)
		@abs = Indicator.new(0, window, "parts/icons/abs.png", 500, 544)
		@air_bag = Indicator.new(0, window, "parts/icons/airbag.png", 423, 534)
		@on_sis = Indicator.new(0, window, "parts/icons/onsis.png", 452, 400)
		@arka_sis = Indicator.new(0, window, "parts/icons/arkasis.png", 555, 395)
		@turn_left = Indicator.new(0, window, "parts/icons/solsinyal.png", 444, 329)
		@turn_right = Indicator.new(0, window, "parts/icons/sagsinyal.png", 559, 329)
		@flashers = Indicator.new(0, window, "parts/icons/dortlu.png", 505, 329)
		@dipped_beam = Indicator.new(0, window, "./parts/icons/kisafar.png", 452, 440)
		@main_beam = Indicator.new(0, window, "parts/icons/uzunlar.png", 445, 480)
		@daytime_running_lamp = Indicator.new(0, window,"parts/icons/gunduzfari.png", 555, 440)
		@handbrake_indicator = Indicator.new(0, window, "parts/icons/elfreni.png", 500, 521)
		@park_lights = Indicator.new(0, window, "parts/icons/parklambasi.png", 500, 500)
		@belt = Indicator.new(0, window, "parts/icons/kemeruyari.png", 555, 470)
	end
	def init_gosu_objects(window, digit_font, string_font)
		@speed_dig = Gosu::Font.new(window, digit_font, 25)
		@km_per_hour = Gosu::Font.new(window, string_font, 15)
		@km = Gosu::Font.new(window, string_font, 15)
		@dist_digit = Gosu::Font.new(window, digit_font, 25)
		@needle = Gosu::Image.new(window, "parts/ibre.png", false)
		@needle_small = Gosu::Image.new(window, "parts/kucuk_ibre.png",false)
		@time = Gosu::Font.new(window, string_font, 10)
		@date = Gosu::Font.new(window, string_font, 10)
	end
	def speed_up
		if @angle_speed < 79.0 and @angle_fuel > -110.0 then
			@angle_speed += 0.1
		end
	end
	def speed_down
		if @angle_speed > @start_angle_speed and @angle_fuel < @start_angle_fuel+20 then
			@angle_speed -= 0.5
		end
	end
	def brake
		if @angle_speed > @start_angle_speed then
			@angle_speed -= 1
		end
	end
	def handbrake_toggle
		@handbrake_toggle ^= 1
	end
	def handbrake_on
		if @angle_speed > @start_angle_speed and @handbrake_toggle == 1 then
			@angle_speed -= 2
		end
	end
	def fuel_down
		if @angle_fuel > -110.0 and @speed > 0 then
			 @angle_fuel -= @speed * 0.00005
		end
	end
	def fuel_up
		unless @angle_fuel > 10
			@angle_fuel += 1
		end
	end
	def speed_calc
		if @angle_speed > @start_angle_speed then
  			@speed = (-(11*(11*@angle_speed.round-7840)*(@angle_speed.round+127))/72036).round
  		else
			@speed = 0
		end
	end
	def show_speed
	  		@speed_dig.draw(@speed, 510, 210, 0)
			@km_per_hour.draw("kmh", 564, 220, 0)
	end
	def distance_calc
		@timers.every(5){ @distance = ((@speed.round(3)/3600.0) + @distance).round(3) }
		@dist_digit.draw((@distance/1000).round(3), 491, 236, 0)
		@km.draw("km", 564, 245, 0)
	end
	def needle
		@needle.draw_rot(230, 415, 1, @angle_round)
		@needle.draw_rot(811, 421, 1, @angle_speed)
		@needle_small.draw_rot(888, 500, 1, @angle_fuel)
		@needle_small.draw_rot(320, 490, 1, @angle_heat)
	end
	def show_date_and_time
		t = Time.new
		@time.draw(" #{t.hour}:#{t.min}",440, 275, 0)
		@date.draw("#{t.day}.#{t.month}.#{t.year}", 550, 276, 0)
	end
	def round_up(times)
		if @speed <= 219 and  @angle_round < 80 then
			@angle_round += 0.1 * times
		end
	end
	def round_down(times)
		if @speed > 0 and @angle_round > -127.0 then
			@angle_round -= 0.1 * times
		end
	end
	def round_on
		if @speed > 0
			if @speed >= 0 and @speed <= 30.0
				round_up(1)
			elsif @speed > 30.0 and @speed < 35
				round_down(3)
			elsif @speed > 35.0 and @speed < 65
				round_up(1)
			elsif @speed > 65.0 and @speed < 70
				round_down(3)
			elsif @speed > 70.0 and @speed < 100.0
				round_up(1)
			elsif @speed > 100.0 and @speed < 105.0
				round_down(3)
			elsif @speed > 105.0 and @speed < 135.0
				round_up(1)
			elsif @speed > 135.0 and @speed < 140.0
				round_down(3)
			elsif @speed > 140.0 and @speed < 170.0
				round_up(1)
			elsif @speed > 175.0 and @speed < 180.0
				round_down(3)
			elsif @speed >= 180.0 and @speed < 219.0
				round_up(6)
			end
		end
	end
	def heat_on
		if @speed > 120 and @angle_heat < 10
			@angle_heat += 0.05
		elsif @angle_heat > -70
			@angle_heat -= 0.05
		end
	end
 	def draw_icons
		@abs.draw
		@air_bag.draw
		@on_sis.draw
		@arka_sis.draw
		@turn_left.draw
		@turn_right.draw
		@flashers.draw
		@dipped_beam.draw
		@main_beam.draw
		@daytime_running_lamp.draw
		@handbrake_indicator.draw
		@park_lights.draw
		@belt.draw
	end
	def draw
		fuel_down
		show_date_and_time
		needle
	 	draw_icons
	 	speed_calc
	 	show_speed
	 	distance_calc
	 	handbrake_on
	 	@timers.fire
	 	heat_on
	end
end
class Indicator
	def initialize(visible, window, icon_file, x, y)
		@x, @y = x, y
		@test = icon_file
		@icon = Gosu::Image.new(window, @test , false)
		@visible = visible
	end
	def toggle
		@visible ^= 1
	end
	def draw
		if @visible == 1 then
			@icon.draw(@x, @y, 0)
		end
	end
	def draw_now
		@visible = 1
		@icon.draw(@x, @y, 0)
	end
	def remove
		@visible = 0
	end
end
window = Window.new
window.show