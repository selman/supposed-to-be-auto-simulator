require 'gosu'
require 'timers'
class Window < Gosu::Window
	def initialize
		super(1024, 768, false)
		@auto = Hyundai.new(self, "font/Crystal.ttf", "font/digi.ttf", "em_gauge.png")
		self.caption = @auto.title
		@background_image = Gosu::Image.new(self, @auto.background_image, true)
	end
	def draw
		@background_image.draw(0,0,0)
		@auto.draw
	end
	def button_down(id)
		if id == Gosu::KbEscape
	  		close
		end
	end
	def update
		if button_down? Gosu::KbUp then
			@auto.speed_up
			@auto.round_on
		else
			@auto.speed_down
			@auto.round_down(5)
		end
		if button_down? Gosu::KbDown then
			@auto.brake
			@auto.abs.draw_now
		else
			@auto.abs.remove
		end
		if button_down? Gosu::Kb0 then
			@auto.park_lights.toggle
		end
		if button_down? Gosu::Kb1 then
		 	@auto.on_sis.toggle
		end
		if button_down? Gosu::Kb2 then
			@auto.arka_sis.toggle
		end
		if button_down? Gosu::Kb3 then
			@auto.dipped_beam.toggle
		end
		if button_down? Gosu::Kb5 then
			@auto.main_beam.toggle
		end
		if button_down? Gosu::Kb6 then
			@auto.belt.toggle
		end
		if button_down? Gosu::Kb7 then
			@auto.air_bag.toggle
		end
		if button_down? Gosu::Kb4 then
			@auto.daytime_running_lamp.toggle
		end
		if button_down? Gosu::KbA then
			@auto.turn_left.toggle
		end
		if button_down? Gosu::KbB then
			@auto.fuel_up
		end
		if button_down? Gosu::KbD then
			@auto.turn_right.toggle
		end
		if button_down? Gosu::KbF then
			@auto.flashers.toggle
		end
		if button_down? Gosu::KbSpace then
			@auto.handbrake_toggle
			@auto.handbrake_indicator.toggle
			@auto.handbrake_on
		end
	end
end
